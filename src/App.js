import React, { useState } from 'react';
import ZoomImage from 'react-img-zoom'
import './App.css';

const flickrImages = [
  'https://home.ripley.cl/store/Attachment/WOP/D191/2000380166950/2000380166950_2.jpg',
  'https://home.ripley.cl/store/Attachment/WOP/D191/2000380166950/2000380166950-1.jpg',
  'https://home.ripley.cl/store/Attachment/WOP/D191/2000380166950/2000380166950-2.jpg',
  'https://home.ripley.cl/store/Attachment/WOP/D191/2000380166950/2000380166950-3.jpg',
  'https://home.ripley.cl/store/Attachment/WOP/D191/2000380166950/2000380166950-4.jpg',
];

function App() {
  const [selectImage, setSelectImage] = useState(flickrImages[0])

  const handleChangeImage = (image) => {
    setSelectImage(image)
  }

  return (
    <div className="App">
      <header className="App-header">
        <div className='d-flex'>
          <div className='list-images'>
            {flickrImages.map(image =>
              <img key={image} src={image} alt='' width='100' onMouseOver={() => handleChangeImage(image)} />
            )}
          </div>
          <div>
            <img src={selectImage} alt='' />
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
